---
title: "Nishat Writes Here..."
date: 2022-11-08T16:57:40+06:00
tags: []
---

Hello. My name Jaynul Abedin Nishat and this is my website. There's a lot here, so thanks for stopping by!
{{<rawhtml>}}
<figure style="align:center">
	<img src="/images/pp.jpg"
             alt="my portrait" style="width:50%">

        <figcaption>I look somewhat like this</figcaption>
</figure>
{{</rawhtml>}}

---
## Latest Blog Posts in English:
- [What is Money?](/english-blog/economics-and-finance/what-is-money)
- [On Dependency Theory](/english-blog/dependency-theory)
- [Bongiyo Bwadiper Otit O Bhobishyot: Dipen Bhattacharya's Take on the Geological Future of Bangladesh](/english-blog/past-and-present-of-bengal-delta)

**[all posts](/english-blog)**

---

## Latest Blog Posts in Bengali:
- [ইতিহাসের অশোকচক্রঃ খাই অফ খেম](/bangla-blog/ইতিহাসের-অশোকচক্রঃ-খাই-অফ-খেম)
- [দ্য ইকোনোমিস্ট এর মতে ২০২২ যেমন হতে পারে](/bangla-blog/দ্য-ইকোনোমিস্ট-এর-মতে-২০২২-যেমন-হতে-পারে)
- [সভ্যতার সংঘাতঃ আন্তর্জাতিক তত্ত্বের একটি থিসিস](/bangla-blog/সভ্যতার-সংঘাতঃ-আন্তর্জাতিক-তত্ত্বের-একটি-থিসিস)

**[all posts](/bangla-blog)**

---

## Latest Study Related Posts:
- [Argument Writing for IELTS etc](/study-related/argument-writing)
- [BCS Resource Locator](/study-related/bcs-study-resource-locator)
- [BCS Past Questions Bangladesh Affairs](/study-related/bcs-past-questions-bangladesh-affairs)

**[all posts](/study-related)**
