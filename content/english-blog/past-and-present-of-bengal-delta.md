---
title: "Bongiyo Bwadiper Otit O Bhobishyot: Dipen Bhattacharya's Take on the Geological Future of Bangladesh"
date: 2022-05-09T14:37:24+06:00
description:
tags: ['book-review']
---

[Dipen Bhattacharya](http://www.bengalpublications.com/writer/dipen-bhattacharya/) is a veteran astronomer who now resides in California where he teaches in the Physics department of the Riverside campus of University of California. He is also a famous Bangla book author, who authored quite a number of fictions in his life, most of which are science-fictions.

This 2017 book of his is published by the University Press Limited, Dhaka. It is also a part of the series of consortium of books titled *Gyan O Sovvota Gronthomala*.

![Dipen Bhattacharya Bongiyo Bwadiper Otit O Bhobishyot](/images/dipen.png)

Although Dipen is a physicist-astronomer by profession, he is very much interested in the Geology of Bangladesh---the formation of this weird and wet homeland of his. This book is his attempt to express to Bangladeshi readers what he found out about this, having studied for all those years---he tells readers in the flap of the book. He also adds his own research findings, especially his findings using SRTM (Shuttle Radar Topography Mission) technology to model the sea-level rise of Bangladesh coast.

The first 70% of the book discusses the geological past of Bangladesh in particular and the subcontinent in general. This part felt like a roller-coaster ride, to be honest. The formation of the land that consists current Bangladesh seemed like a dramatic plot of how one big landmass (Pangaea) got broken into several large pieces and how one of those pieces hit another big landmass (Asia) and formed mountain ranges on both north and east of that breakaway land-piece. And it is fantastic that we can easily verify this story with the current scenarios present all around Bangladesh.

That [Dinajpur](https://en.wikipedia.org/Barapukuria_coal_mine) and [Sylhet](https://www.wikiwand.com/en/Sylhet#/Economy) hold huge reserves of fossil fuel not-so-deep inside their land, can actually be predicted---as a matter of fact, it WAS predicted. That's how geologists find fossil fuels and other mineral resources, not---as I often wondered---by simply boring holes here and there.

The book also speaks of the risks of Bangladesh's geological position. Do you know, why most of the time most of the rivers in Bangladesh, while traveling from the North to the South in the Bay of Bengal, leans towards the east? It is because the subcontinental plate (breakaway from Pangaea) actually subsides under the Indo-Burman plate. And that is also why Burma and Tripura have that many mountains and why the heights of the mountains are increasing slowly---because this subsidence is a dynamic process.

The author tells a story of sea beaches in Mymensingh and Rangpur where Dinosaurs probably flocked together for entertainment purposes! He also talks about the height problem of the Himalayan mountain range. He gives the reader a tour of of the regions of Rajshahi, Natore, Khulna, Barisal, Satkhira and of course, Chattogram, while explaining their past and present. He speaks of Active Delta of Barisal and Inactive Delta of Sundarbans, and of the dead Delta Varendra.

The book also touches the history of first settlements of humans in this region, while denoting that this region was more or less always a densely populated region. Because there is this dynamism between constant sedimentation and subsidence (under mountains) of the land, the author maintains that this Delta has been one of the most fertile regions in the Earth since at least 10,000 years ago, civilizations and population centers were regularly formed and consequently destroyed by flooding and oceanic cyclones. It is a grim story, but deems true.

The author also recommends some actions to take both by the government and development partners and by academia, for the betterment of this land. He suspects that, dams constructed throughout the country to ward off floodings is not helping the land formation---which will prove harmful later in the near future. He also is not so much of an advocate for dams on rivers---so he advises building strong diplomatic policy stance to negate the few dam projects of India and China for the betterment of Bangladesh.

In fine, this read was a fantastic little ride through a jungle, the trees of which are mostly unknown to me. The contents are fairly technical, but that actually is quite necessary to cover this topic. Only interested readers should try this.
