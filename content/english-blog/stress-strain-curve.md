---
title: "Why the Stress-Strain Curve is called a Stress-Strain Curve and not a Strain-Stress Curve?"
date: 2022-05-08T00:43:21+06:00
description: The famous stress-strain curvature / diagram seems to have a problem regarding its axes, or does it?
tags: ['engineering', 'science']
---

The stress vs strain curve is a famous and useful diagram/curve used widely in material science, engineering and/or physics to describe the relationship between, well, the stress and the strain.

For those who don't know a thing about all these, I've prepared a background discussion here:

### Background Discussion
#### What is Stress?
When some amount of load (or force) is applied on a body the pressure that gets produced due to loading is called, in simple terms, the stress.

It has unit of \\(\frac{N}{m^2}\\)---SI unit that is. Which is just the unit of pressure, no? Yes---stress is kind of the same as the pressure on a body. This unit is also known as `$Pa$` (Pascal).

Let's consider the figure below.
![Fig 1](/images/stress-strain_1.png)
Here the body is being applied a force of `$P = 25 N$`. The area (at which the load is being applied) is: `$A = 50 \times 50\ mm^2 = 25\times 10^{-4}\ m^2$`.


So, the stress here is:

`$\sigma = \frac{P}{A}$`

`$\implies \sigma = \frac{25}{25\times 10^{-4}} \frac{N}{m^2}$`

`$\therefore \sigma = 10,000\ Pa = 10\ kPa$`

#### What is strain?

Strain is just the ratio of the deformation of a body to its original length, which is under a certain external load (and thus, certain external stress).

Strain has no unit. It is thus called dimensionless.

The body with length `$L$` shown below (phase 1)---upon being applied external stress---`elongates` to length `$L'$` (phase 2). So, the elongation becomes `$\delta = L' - L$`.

![](/images/stress-strain_2.png)

Now the strain will be:

`$\epsilon = \frac{\delta}{L}$`

### So what's all the fuss about?

Well, we generally
