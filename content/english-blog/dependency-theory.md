---
title: "The Dependency Theory"
date: 2022-06-07T22:30:52+06:00
description: The Dependency Theory is one of the key theories in International Relations and Global Studies. In this post I will try to succintly explain this theory.
tags: ['geopolitics', 'international-affairs']
---

## Background:
The Dependency Theory emerged in the 1960s and the 1970s in the South and Central Americas. But it was part of a larger movement---in that it was asking questions about International Relations at that time.

And one of those questions was:
> Why are so many countries in the world NOT developing?

Now, the traditional answer to that question was:
> Well, because these countries were not pursuing the right economic policies.

or
> Their government are authoritarian and corrupt.

But dependency theorists wanted to know if that was all there to it.

And they began to argue that, in fact, countries weren't developing around the world because the INTERNATIONAL SYSTEM was actually PREVENTING them from doing so. They were arguing that in fact, this International System was exploititive. It was characterised by the dominance of some countries over others.

## Main Arguments of The Dependency Theory:
There are many different ways to describe the theory. What I have done here is more of an assembly of different sorts of ideas including World Systems Theory, Historical Structural Theory and Neo-Marxist Theory. There are components of these theories in the Dependency Theory.

Now the Dependency Theory first of all argues that there are a number of different kinds of states in the world, and each of them performs different types of functions accordingly.

![](/images/dt1.png)

First of all, there are the countries that are the Center of the Center (CC)---these are the richest, most powerful countries of the world---the United States, the United Kingdom, France and so on.

Then there are countries that are on the Periphery of the Center (PC)---these are modern industrialised wealthy countries like Canada for example, or the Netherlands and perhaps Japan. And these countries have a little less Global Power than the CC countries do, but nevertheless, they are quite rich countries.

Then you have countries that are the Center of the Periphery (CP)---these are countries that are still developing, but they have a fair amount of wealth. These are countries like South Africa, India or Brazil.

Then you have countries that are at the Periphery of the Periphery (PP)---these are the poorest countries of the world: Zambia, Cambodia, El Salvadore etcetera.

### The First Argument: International Division of Labour
The first argument of the dependency theorists is that there is an International Division of Labour between all of these countries. And their argument is that the core countries the Centers of the Centers (CC) dominates in terms of industries, technology---they are the countries are are rich in technology, research and capital-intensive technologies. Meanwhile countries in the periphery are pretty much characterised by resource extraction economies, agricultural production and providing cheap labour.

And as a result, the structure of the world economy is one in which the PC countries serve the interests of the CC countries. The countries at the periphery of the center (PC) serve both the interests of the core countries(CC) and the periphery of the center (PC) countries. And naturally the countries at the periphery of the periphery (PP) serve the economic interests of all the others (CC, PC, CP).

![](/images/dt2.png)

### The Second Argument: Class Distinction
The second argument the dependency theorists make is that there is a Class Distinction.

In each of these different type of countries, there is a clear divide between the rich and the poor.

The rich people---the political and economic elites of the countries around the world---all cooperate with one another. They cooperate with one another to ensure that they stay in power and that they increase their own wealth. So, they collaborate with one another to maintain the system, to keep the system the way it is.

![](/images/dt3.png)

### The Third Argument: Global Capitalism
Third argument made by the dependency theorists is that all of these structures, the International Division of Labour, the Class Divisions---all exists in a wider global system. This wider global system is characterised by Global Capitalism.

What that means, is that, in this system Liberal Economic Theory dominates. Theories of Trade, theories of Finance dominate this global system---which of course serve the purpose of those CC countries. In addition, Multinational Corporations (MNCs), International Banks---these are the instruments of rich people in the CC countries. International Institutions, like the World Bank, the International Monetary Fund all serve the interests of the richest people of the richest countries of the world. Even things like Education System and the Global Media also are instruments that serve the purpose of the CC countries and their elites.
![](/images/uploads/dt4.png)

## Conclusion
And so as a result, this entire system---(1) International Division of Labour; (2) Class Distinction and (3) Global Capitalism all serve the interests of the wealthy. They don't serve the interests of developing countries. They do not promote development or equal opportunities. Instead the system promotes dominance and exploitation.

So, from a dependency theory perspective, how can the countries develop in this system? The system is actually designed to prevent them from developing and dependency theorists call this: UNDERDEVELOPMENT. And so, the system promotes Underdevelopment of countries around the world. This is why we donot see economic developments of many countries in the world today.
