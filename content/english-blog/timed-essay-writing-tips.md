---
title: "Tips for writing TIMED essays"
date: 2022-04-29T22:16:10+06:00
description: Writing a timed essay is a virtuoso skill that must be practices... This is a consortium of advices and blueprints given by many learned.
tags: ['writing']
---

The only way to survive here is:
# "PRACTICE TIMED ESSAY" (at least 5 times a week)

## The way to do this is:
### 1. KEEP IT SIMPLE.
it's a timed essay... not an academic journal paper
### 2. PRACTICE.
we usually tend to overthink and complicate things when we write... the thoughts in our head are extremely hard to articulate properly on paper... only practice makes this head-to-paper transition fluent.
### 3. Once you feel comfortable with the work of writing itself... RESEARCH THE MATERIALS.
that is... you should collect and look for materials including informations, data, graphs, charts etcetera that will apply to pretty much everything encircling an issue... for example, the GDP growth rate of the country during the last 5 or so years is a very applicable information to hold onto --- it can be applied to make so many different points!

## While writing, do these:
### 1. MAKE AN OUTLINE BEFORE STARTING TO WRITE.
take at least 5 minutes to think and work out a rough outline of what you will have to write as your main-theme and topic sentence. jot down key informations that you will write on your key paragraphs. leave introduction and conclusion alone. this outline will be your algorithm.
### 2. THINK BEFORE WRITING INTRODUCTION AND CONCLUSION (looking at your outline and topic-sentence)
make up at least 5 minutes for each
### 3. DO NOT WRITE ANYTHING THAT WILL NOT PROVE YOUR POINT.
just don't do that. it's a timed test, not a book.
