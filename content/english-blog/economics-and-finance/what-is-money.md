---
title: "What Is Money?"
date: 2022-11-09T22:00:56+06:00
tags: ['economics', 'finance', 'debt']
---

_This is the first of a three part series in which I want to question how our economics work and in the process of thinking about all that, i hope, we will be able to discern to make good personal decisions_

## What is Money?

In simple terms, money is a thing that facilitates transactions. Then, what is a transaction? A transaction is the exchange of values. What are values? Values are, generally things of importance or need in our life---which can be both goods and services.

In the aftermath of the [Agricultural Revolution](https://www.wikiwand.com/en/First_Agricultural_Revolution) at the onset of Neolithic period, when people (archaic people that is) gathered together to form Families and communities, there emerged a necessity of a ubiquitous medium of transaction among the people who used to [Barter](https://www.wikiwand.com/en/Barter) theretofore. Then, probably in Levant first, people invented "neutral" media of transaction. Media which by itself doesn't add much value to one's life, but can be "trusted" by a lot of people to allow the media to be transacted in exchange of values (and services).

Throughout history, a whole range of things were used as money, including gold, silver, brass and even grains (in ancient Mesopotamia). Then since the 7th century China, usage of paper as money was becoming widespread. These all money forms can be termed as Physical Money. In fact I want to divide the current forms of money into two forms:

![](/images/money_1.png)
