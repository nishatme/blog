---
title: "BCS Past Questions: Bangladesh Affairs"
date: 2022-03-30T00:33:54+06:00
description: A collection of questions that appeared in BCS exam in the Bangladesh Affairs paper.
tags: ['study-related', 'bangladesh-affairs']
---

### Syllabus

- Subject: **Bangladesh Affairs**
- Subject Code: 005
- Type: Compulsory Written
- Total Marks: 200
- Time: 4 hours

The paper consists of various things related to Bangladesh. The topics that are included in studying Bangladesh are:

1. **Geography of Bangladesh** that should include topographical features of different areas/regios and their developments over time.
2. **Demographic Features** including ethnic and cultural diversity.
3. **History and Culture of Bangladesh** from ancient to recent times.
4. **Economy, Society, Literature and Culture of Bangladesh** with *particular emphasis* on developments including Poverty Alleviation, Vision-2021, GNP, NNP, GDP etcetera after the emergence of the country.
5. **Bangladesh's environment and nature and challenges and prospects** with particular emphasis on conservation, preservation and sustainability.
6. **Natural Resources of Bangladesh** with focus on their sustainable harnessing and management.
7. **The Constitution of the People's Republic of Bangladesh**: Preamble, Features, Directive Principles of State Policy, Constitutional Amendments.
8. **Organs of the Government**:
    1. *Legislation*: Representation, Law-making, Financial and OVersight functions; Rules of Procedure, Gender Issues, Caucuses, Parliament Secretariat.
    2. *Executive*: Chief and Real Executives, eg. President and Prime Minister, Powers and Functions, Cabinet, Council of Ministers, Rules of Business, Bureaucracy, Secretariat, Law Enforcing Agencies; Administrative setup of National and Local Government, Decentalization Programmes and Local Level Planning.
    3. *Judiciary*: Structure: Supreme, High and other Subordinate Courts, Organization, Powers and Functions of the Supreme Court, Appointment, Tenure and Removal of Judge, Organiation of Sub-ordinate Courts, Separation of Judiciary from the Executive, Judicial Review, Adjudication, Gram Adalat, Alternative Dispute Resolution (ADR).
9. **Foreign Policy and External Relations of Bangladesh**: Goals, Determinants and Policy formulation process; Factors of National Power; Security Strategies, Geopolitics and Environment Issues, Economic Diplomacy, Manpower exploitation, Participation in International Organizations; UNO and UN Peace Keeping Missions, NAM, SAARC, OIC, BIMSTEC, D-8 etcetera and International Economic Institutions, Foreign Aid, International Trade.
10. **Political Parties**: Historical developments; Leadership; Social Bases; Structure; Ideology and Programmes; Factionalism; Poltics of Alliances; Inter and Intra-Party Relations; Electoral Behaviour; Parties in Government and Opposition.
11. **Elections in Bangladesh, Management of Electoral Politics**: Role of the Election Commission; Electoral Law; Campaigns; Representation of People's Order (RPO), Election Observation Teams.
12. **Contemporary Communication**: ICT, Role of Media; Right to Information (RTI), E-Governance.
13. **Non-formal Institutions**: Role of Civil Society, Interest Groups; NGOs in Bangladesh.
14. **Globalization and Bangladesh**: Economic and Political Dimensions, Roles of the WTO, WOrld Bank, IMF, ADB, IDB and other development partners and Multi National Corporations (MNC).
15. **Gender Issues and Development in Bangladesh**.
16. **The Liberation War and It's Background**: Language Movement 1952, 1954 Election, Six-Point Movement 1966, Mass Upsurge 1968-'69, General Election 1970, Non-cooperation movement 1971, Bangabandhu's Historic Speech on 7th March, Formation and Functions of Mujibnagar Government, Role of Major Powers and of the UN, Surrender of the Pakistan Army, Bangabandhu's Return to Independent Bangladesh, Withdrawal of Indian Armed Forces from Bangladesh.


### Questions:
#### 1:
- Write short note on: Bangladesh's Maritime Boundary Victory.
- Describe the geographical location and geography of Bangladesh.
- Discuss the strategic advantages and risks of Bangladesh in geopolitics.
- Write short note on: Solving the enclaves problem.
- Describe in details the adverse effects that might fall upon Bangladesh due to Climate Change.
- Write the importance of Coral Island in Bangladesh's geography.
- What do you understand of the Inactive Delta of Bangladesh?
- Discuss the importance of Varendra Region and the Varendra Museum in Geography of Bangladesh.
- Describe briefly the significance of Bangladesh being geogrphically located in South Asia.
- What are the economic importance of the Bay of Bengal?
#### 2:
- What do you understand by "Family Planning?"
- Describe the steps taken by the government in Population Control.
- Describe the steps taken and successes of the government of Bangladesh in transforming populace to human resources.
- Whom do you understand by ethnic minorities?
- Evaluate the contribution of the ethnic minorities in Bangladesh's economic and social sectors.
- Are there any step taken by the government to develop the lifestyle of the ethnic minorities?
- What is meant by Human Resource?
- 'Education is the most important to develop skilled Human Resource' - discuss.
- How much is the attempts of the government in development of proper education?
- Write short note on: Union Health and Family Welfare Center.
- What are the differences between Race and Nation?
- Bengali are a mixed race - explain.
- Describe the social and cultural lifestyle of the Chakmas in CHT.
- Describe shortly the Rarh Janapada of ancient Bengal.
#### 3:
- Write short note on: National Museum of Bangladesh.
- Write short note on: Martyred Intellectuals Day
- What was Permanent Settlement?
#### 4:
- Explain with discussion the role of the relation among Education, Society and Economy to the overal development of Bangladesh.
- Discuss with examples how the large number of population of Bangladesh can be transformed into Human Resources to Economically advance the country.
- How are EPZs playing its part in Industrial Development of Bangladesh?
- What are the differences between BEZA and BEPZA?
- What are some of the completed projects under PPP?
- Discuss the role of the agriculture sector on the economic development of Bangladesh.
- Describe the steps taken by the government and the successes in agricultural development and improving the living standard of farmers.
- Write short note on: Vision-2021.
- Write short note on: Bangladesh's Food Security.
- What is meant by Poverty Alleviation?
- Describe brifly the steps taken by the government to eradicate extreme poverty.
- What contributions the Remittance Sector are making on the Economic development of Bangladesh?
- Discuss five hindrances in Industrialisation of the country.
- Briefly describe Bangladesh's progress in Social Development Index.
#### 5:
- What is meant by Disaster Management?
- 'Despite being a developing country, Bangladesh is quite capable of disaster management'- discuss.
- Do you think, environmental pollution is one of major reasons of natural calamities?
- Discuss the environmental effects of Teesta Water Crisis.
- What do you understand by Natural Disaster in Bangladesh?
- In your view, what are the major reasons behind River Pollution.
- Reflect on a overview of waste management in Bangladesh.
- What effects can on Bangladesh be fallen due to Sea level rise, in future?
#### 6:
- State your advices regarding the importance of preservation of Bangladesh's forests and forest resources, analyzing the steps taken by the government.
- State the importance of Bangladesh's forest resources.
- Explain the concept of "Sustainability". Discuss with respect to Bangladesh the importance of and the means of "sustaining" in current world.
- Write short note on: Proper usage of Gas resources
- Write short note on: Blue economy.
- Write short note on: Green economy.
- Discuss the mineral resources of Bangladesh.
- What are the problems and possibilities of extracting our mineral resources and using them in industries?
- Discuss the importance of coal as an energy resource.
- Write short note on: Our maritime resources.
- Write short note on: PPP (Public-Private Partnership).
#### 7:
- Define 'Constitution'.
- When was the constitution of Bangladesh first put into force?
- Discuss the fundamental principles of Bangladesh's Constitution.
- Discuss the fundamental principles of state policy described in the constitution.
- Discuss the fundamental principles of the Constitution of 1972.
- What 'version' of the constitution of Bangladesh is followed today? Discuss the significance of that.
- Describe the laws regarding amendments to the constitutions.
- Describe the fifteenth amendment of the constitution of Bangladesh.
- How many articles are there in Bangladesh's constitution?
- Describe the formation, powers and functions of Jatiya Sangsad.
- How logical it is to conserve seats for women in the parliament?
- Discuss briefly the appointment process and functions of Attorney-General.
- What are the fundamental rights of the citizens according to the Constitution?
- Write down the rights of women reserved by the constitution.
- "All powers in the Republic belong to the people" - explain this reflecting to the concerned article in the constitution.
- What is a Constitutional Post? Name 5 constitutional posts along with their functions.
- Write down the political background and importance of: 5th Amendment.
- Write down the political background and importance of: 12th Amendment.
- Write down the political background and importance of: 15th Amendment.
- Write down the political background and importance of: 16th Amendment.
- Write the Article 12 of the Constitution of Bangladesh regarding Secularism.
- What type of amendments do you propose in the Article 70 of the constitution?
- How does a constitutional government form?
- How does the election commission form?
- What are the major functions of election commission?
- Explain the importance of an Ombudsman in Bangladesh.
- Give a description of the constitutional powers of the prime minister.
#### 8:
- Write short note on: Administrative tribunal.
- Describe in details the roles of the Legislative, the Executive and the Judiciary on smooth running of the state, according to the Constitution of the People's Republic of Bangladesh.
- Discuss the structure of local government in Bangladesh.
- Discuss the "Hilltracts District Council".
- How many guilties had been given capital punishment till now under the International Crimes Tribunal? Whose capital punishment had been in effect?
- Write short note on: Income Tax Fair.
- What are the differences between Act and Ordinance?
- Write about the formation of the legislative council.
- Write about the importance of the "Rules of Procedure" in the administration of Jatiya Sangsad.
- Write about the formation of High Court of Bangladesh.
#### 9:
- Discuss the bases of Bangladesh's foreign policy.
- "Upholding the national interest is the main determinant of Bangladesh's foreign policy" - explain.
- Discuss the role of Bangladesh Army on UN Peace Keeping Missions.
- What roles can the Foreign Ministry of Bangladesh play in order to send back Rohingyas who are staying now in Bangladeh back to Myanmar?
- Discuss the determinants of Bangladesh's foreign policy formulation.
- According to current situation, how could the relation among Bangladesh, India and China be? Give opinion.
- Write short note on: SAARC and Bangladesh.
#### 10:
- Write short note on: Role of political parties on Democracy.
- What is Leadership?
- Discuss the charismatic leadership of Bangabandhu Sheikh Mujibur Rahman.
- Formation of Alliances is not new among Bangladesh's political parties - discuss.
- What are the existing party systems in Bangladesh?
- Write about the significance of removing "Muslim" from "Awami Muslim Leage" in 1955.
- Write briefly the functions of Political Parties in Bangladesh.
- What are major characteristics of opposition parties after 1991?
#### 11:
- What do you mean by Representation of People Order (RPO)?
- Discuss the role of Election Commission on conducting General Elections.
- What roles can Election Observers play to ensure fair elections?
- Discuss the role of Election Commission on spreading the democratic process.
- In case of eradicating corruption, reducing the election costs is a panacea - opine.
- Describe the importance of Electoral Observers in case of Elections in Bangladesh.
- Present a view of the election spendings in Bangladesh in the last consecutive elections since 2001.
#### 12:
- Write short note on: Digital Bangladesh
- What is meant by ICT?
- Analyze the progress of Bangladesh in ICT.
- How much do you think is the possibilities of ICT in the country in near future?
- In which of the sectors the government is trying to forward digitalization process to forefronts of people, to achieve socio-economic progress?
- What is meant by E-Tendering? Since when E-Tendering has been in effect in Bangladesh?
- Write short note on: E-Governance and Bangladesh.
#### 13:
- What do you understand of Civil Society?
- State the role of Civil Society in formulation of Public Opinion.
- Discuss the differences between NGO and Civil Society.
- Discuss the role of NGOs on Women Empowerment in Bangladesh.
- Write short note on: Good governance.
- Write short note on: Freedom of Newspapers.
#### 14:
- Write short note on: Economic Diplomacy.
- Write short note on: Export of Bangladesh and the WTO
- Write short note on: IMF in Bangladesh
- Write about the importance of Bangladesh's participation in the UN Peace Keeping Missions in terms of economy.
- What is BCIM? What are its major significance in increasing regional cooperation.
- Write short note on: International Labor Market and Bangladesh.
- Write short note on: World Cricket and Bangladesh.
- Write short note on: Bangladesh and WTO.
#### 15:
- "Prithibite ja kichu mohan sristy chiro-kollyankor\\Ordhek tar koriyache nari, ordhek tar nor." How much of the current social situation of Bangladesh is relevant to this quote by the National Poet Kazi Nazrul Islam? Give opinion in your own words.
- Explain with examples the importance of ensuring equal status of women and women empowerment for social and economic development of Bangladesh.
- Write short note on: Women empowerment.
- Write short note on: Bangladesh's position in Women empowerment.
- What are 5 notable steps taken by the current government in women empowerment.
- Discuss the names, their contributions and the sectors they fought in of 3 awarded woman freedom fighters.
- When was the first woman judge appointed in the Supreme Court? Write her short biography.
- Describe the international awards of the current prime minister of Bangladesh.
#### 16:
- What are the effects of 1952 Language Movement on the escalation of the Liberation War.
- What are the significance of the 1952 Language Movement on Bangladesh's Liberation War.
- What are the effects of the 7th March speech on the Liberation War of Bangladesh.
- What are the effects of the '69 Mass Uprising on the Liberation War.
- Which institution declared the Historic speech of 7th March as World Documentary Heritage?
- State the different phases of Bangladesh's Liberation Struggle during 1947 and 1971.
- State the role of "Six-point Movement" on Bangladesh's Liberation Struggle.
- State the role of "69 Mass Uprising" on Bangladesh's Liberation Struggle.
- State the role of "70's General Election" on Bangladesh's Liberation Struggle.
- Write short note on: 70's General Election
- State the role of "7th March Speech" on Bangladesh's Liberation Struggle.
- Where and why the Mujibnagar Government took oath?
- Write short note on: Mujibnagar Government.
- Discuss the roles played by USA, then Soviet Union, China and India in Bangladesh's Liberation War.
- What are the significance of the Instrument of Surrender?
- What are the difference between Liberation Struggle and Liberation War?
- Write the names of Bir Shresthos.
- Write short note on: 17th April 1971.
- The Return of Bangabandhu Day - discuss.
- Withdrawal of Indian Armed Forces from the newly liberated Bangladesh - analyse.
- What were the 6 points of Awami League?
#### Recent Affairs:
- (41st) Discuss the difference between Epidemic and Pandemic.
- (41st) How much has Bangladesh's economy and health sector been affected by the Covid-19 pandemic? Discuss.
- (41st) Discuss the role of local government organizations in fighting pandemic.
- (41st) Write short note on: Road Accidents and Bangladesh.
- (40th) What is War Crime?
- (40th) Describe the significance of trial of war crimes, first discussing it.
- (40th) Discuss with examples the reactions in investigating criminal cases exerted herein following the formation PBI.
- (38th) Who are Rohingyas?
- (38th) Discuss the recent problems that has been created in Bangladesh due to massive amount of Rohingya infiltration.
- (38th) Is there a solution to the Rohingya problem?
- (36th) Discuss the Local Government (Municipality) Election Rules - 2015.
- (36th) Analyse the "Union Council Election -2016".
- (36th) What are the basic goals of the 7th five-year plan?
- (36th) Discuss the development goals of the Bangladesh government to be achieved within 2021.
- (36th) What do you understand of Implementation of Padma Bridge Project?
- (36th) Reflect on the prospects and progresses of the Metro Rail Project.
- (36th) Write short note on: National Education Policy - 2010.
- (35th) Describe the GDP in the last consequtive five years.
- (35th) Write short note on: Quota in government jobs.
