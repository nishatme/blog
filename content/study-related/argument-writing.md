---
title: "Argument Writing"
date: 2022-11-12T21:47:41+06:00
tags: ['english', 'writing', 'ielts']
---

### Sentence \\(\rightarrow\\) a group of words. eg, _My name is Ryan._

### Paragraph \\(\rightarrow\\) a group of sentences, that:
1. starts with an indentation, and
2. are about the same topic.

eg:

\\(\ \ \ \ \\)My name is Ryan. I am from Ottawa, Canada. I have been living in Dubai for 2 years. I speak three languages, namely---English, Arabic, Spanish. I had been to five capitals in the last two years---London, Washington, Ottawa, Dubai and Asghabat. As you might have already guessed, I do like to travel a lot. But I also have a knack for programming and general IT related stuffs.

### Essay \\(\rightarrow\\) a group of paragraphs. _important things to remember when writing essay are:_
1. each paragraph is different,
2. each paragraph has its own job.
3. an essay has at least 4 paragraphs. Introduction, Body 1, Body 2, Conclusion.
4. Body 1 and Body 2 paragraphs have different arguments than each other.
5. Introduction is a statement of introduction.
6. Conclusion is a restatement or the summary of the arguments in Body 1 and Body 2.

#### Introduction:
1. has to have 4 specific sentences
2. has to have 4 different ideas
3. sentence 1: Background statement (gives some general information about our topic)
4. sentence 2: Detailed Background statement
5. sentence 3: Thesis sentence (most important sentence in the entire essay) \\(\rightarrow\\) what we are trying to prove | eg. _(Yes) Dubai is the most expensive city to live in_
6. sentence 4: Outline sentence (tells the reader what topics we're gonna speak of in the Body 1 and Body 2 paragraphs) (should contain the topics of Body 1 and Body 2)

#### Body 1:
1. sentence 1: Topic sentence (subject that supports our thesis) | eg. _talk about real estate, how it is expensive in dubai_
2. sentence 2: Example sentence(s) (must be true in real life) \\(\rightarrow\\) evidences supporting the topic| eg. _real estate in dubai has risen in cost by 100 pc in 2 years_
3. sentence 3: Discussion (hardest sentence in the paragraph to write) (links the example to the topic) | eg _real estate is one of the factors that are making dubai the most expensive city in the world_ or _clearly this is saying that dubai is the most expensive city in the world_
4. sentence 4: Conclusion (links the topic to the thesis) | _cost of real estate is making dubai most expensive city in the world_

#### Body 2:
1. same outline as Body 1
2. except that it has a different topic
3. ie. same _Topic, Example, Discussion, Conclusion_ sentences
4. _body 2 might be about transportation_

#### Conclusion:
1. sentence (1-2): Summary (retelling the two supporting topics briefly)
2. sentence 3: Restatement of thesis (saying the thesis in different words)
3. sentence 4: Prediction or Recommendation, whichever fits the essay-topic
