---
title: Analysis of the Progress of different Stimulus Packages of BB and the
  Government till December 2021
date: 2021-12-15T19:48:15.061Z
description: Data from BB's website and newspapers were used to come to a
  conclusion on the subject matter at hand.
tags: ['study-related', 'economics']
---


*Stimulus* can be defined as those financial packages or concessions given by the Government to sectors of the economy for the sectors to accelerate in growth, motivate itself to grow and to rebound from losses.

Along with 12 packages from the banking sector, Government has given a total of **28 stimulus packages worth 1 Crore 38 Lakh Crores of Taka**. This is a very large amount -- a **near 4.5 pc of the whole GDP** of the country!

<u>**Sectors given stimuli:**</u>

Recent surges of Coronavirus created the necessity of giving this much amount of stimili to the economy. The packages include almost all sectors of the economy including formal and non-formal sectors. These include, packages to be implemented in:

1. Garment Industries
2. Other Manufacturing Industries
3. Low income populace who have fallen in unexpected unemployment recently
4. Imaam-Muazzin of almost every Mosque
5. Teachers of non-MPO educational institutes
6. Orphan students from Kaumi Madrasah
7. Farmers
8. Expatriates who recently lost jobs and came back to Bangladesh
9. Rickshaw-wala, Van-wala, Day Labourers, Construction Workers, Agricultural Workers, Workers of Shops etc.

Of those stimulus packages, the stimulus packages for Businesses is being implemented by Bangladesh Bank alone.

![Stimulus Implementation Rates & Hurdles, as depicted in The Daily Star, Nov 10, 2021](/images/uploads/pic-selected-211216-1547-20.png)

* Banks are **less keen on giving out loans** under the second round of stimulus package, as many **clients are in trouble to pay off** their current debts, a development that may hurt the economy.
* Lenders are also cautious in lending as the b has **beefed up monitoring** on how stimulus funds are being used.
* Banks disbursed only **9 pc of Tk 20,000 crore** among the borrowers of cottage, micro, small and medium enterprises (**CMSMEs**) between July and October, according to data from Bangladesh Bank.
* Bankers say **a large number of clients** have recently sought the **renewal** of their current loans taken from the stimulus packages as they are unable to pay installments due to the ongoing **business slowdown**.
* Similarly, **banks** are also **reluctant** to **disburse loans** to the borrowers of **large industries** and service units.
* In the first four months of the current fiscal year, banks disbursed **14.6 per cent against the central bank's allocation of Tk 33,000 crore for the sector**. The **repayment tenure** of the loans, disbursed in the form of working capital, under the two packages is **one year**.
* The **business slowdown** is still **hammering the economy** despite the sharp declines in coronavirus infections. As a result, borrowers can't repay loans on time.
* But Syed Mahbubur Rahman, managing director of Mutual Trust Bank, thinks: "*There is no way but to provide the loan renewal facility to clients for the sake of the business sector*."
* The disbursement rate of the stimulus loans aimed at large and CMSME borrowers was 81.7 per cent and 77 per cent, respectively, in the first round, which was implemented from May last year to June this year.
* The **two stimulus packages collectively** received **Tk 60,000 crore in the first round and Tk 53,000 crore in the second phase**.
* The **interest rate** on the **stimulus loans** is **9 per cent**, with **large borrowers** **accessing** funds at **4.50 per cent** and the **firms** in the **CMSME sector** at **4 per cent**. The **government** is giving **subsidies** to implement the packages.
* The dismal lending scenario prompted a majority of lenders to inform the central bank in the first week of October about the difficulty in disbursing fresh loans to CMSMEs. On October 20, the central bank, however, ordered the banks to lift the disbursement rate to at least 50 per cent by December.
* In another **discouraging development**, **default loans** in the banking sector have been **on the rise**, handing a blow to lenders to give fresh funds.
* On **September** 14, the **central bank** also unveiled the second round of stimulus funds amounting to **Tk 3,000 crore for the farm sector**. It is yet to avail any information linked to loan disbursement under the package.
* In the first phase, the sector got a stimulus fund of Tk 5,000 crore, of which 85 per cent was disbursed between April last year and June this year.
* In April last year, the **BB unveiled another refinance scheme worth Tk 3,000 crore**, a **revolving fund** for **three years**, for the **marginalised groups** to help them survive amidst the pandemic.
* But, banks are yet to implement it fully: **77 per cent** of the fund has been **given out** so far.
* Banks also displayed a **disappointing performance** to materialise the **pre-shipment refinance scheme** as they disbursed **only 8.57 per cent of Tk 5,000 crore**. The central bank announced the three-year scheme last year.
* But there are hopes that **demand for loans would increase** as the economy is getting back its tempo.
* **Since the inception of the pandemic**, the **central bank** has **rolled out** **10 stimulus packages involving Tk 160,000 crore**.
