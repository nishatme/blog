---
title: "BCS Study Resource Locator"
date: 2022-04-23T00:44:09+06:00
description: "this is the metadata of whereabouts of the study for 43rd bcs..."
tags: ['study-related']
---

# Science:
1. **Acid, Base and Salt:**
	- ssc science (pp 156 onwards)
	- viii science (pp 109 onwards)
	- [khan academy---india playlist](https://youtu.be/Y4HzGldIAss?list=PL2ub1_oKCn7pzMd76PEqtOuGHZ96vO42T)
	- pH: ssc chemistry (pp
2. **Water:**
	- ssc science (pp 40 onwards)
	- ssc chemistry (pp 231 onwards)
3. **Atmosphere:**
	- ssc geography (pp 76 onwards)
4. **Our Resources**:
	- ssc science (pp 172 onwards)

# Bangladesh Affairs:

1. Geography of Bangladesh: **SSC geography**
2. Demographic Features: **VIII BGS**
3. History and Culture of Bangladesh: **VII BGS**
4. Economy, Society, Literature and Culture:
5. Bangladesh Environment, Nature, Challenges and Prospects:
6. Natural Resources of Bangladesh:
7. The Constitution:
8. Organs of Government:
9. Foreign Policy and External Relations:
10. Political Parties:
11. Elections in Bangladesh:
12. Contemporary Communication:
13. Globalisation and Bangladesh:
14. Gender Issues and Development:
15. The Liberation War and It's Background:
