---
title: Sector Commanders of Bangladesh Liberation War 1971
date: 2021-10-15T11:43:10.419Z
description: All of the sector commanders... Listed
tags: ['study-related', 'bangladesh-affairs']
---
#### All eleven sectors of Bangladesh Liberation War and their Sector Commanders:

| Sector | Commander(s)                                                 |
| ------ | ------------------------------------------------------------ |
| 1      | i) Major Ziaur Rahman<br />ii) Captain Rafikul Islam         |
| 2      | i) Major Khaled Mosharrof<br />ii) Major A T M Hayder        |
| 3      | i) Major K M Shafiullah<br />ii) Major A N M Nuruzzaman      |
| 4      | Major Chitta Ranjan Dutta                                    |
| 5      | Major Mir Shawkat Ali                                        |
| 6      | Wing Commander Md Khademul Bashar                            |
| 7      | i) Major Nazmul Hoque<br />ii) Major Kazi Nuruzzaman         |
| 8      | i) Major Abu Osman Chowdhury<br />ii) Major M A Manjur       |
| 9      | i) Major M A Jalil<br />ii) Major Joynal Abedin              |
| 10     | None                                                         |
| 11     | i) Major Ziaur Rahman<br />ii) Major Abu Taher<br />iii) Flight Lt. M Hamidullah Khan |
