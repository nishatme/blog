---
title: "Monetary Policy 2021-22: A Bullet Pointed Summary"
date: 2021-12-12T19:02:38.283Z
description: The title says it all.
tags: ['study-related', 'economics']
---
## A Review of the past FY2020-21's Monetary Policy:

To address the possible damages to the economy due to the worldwide spread of the Corona-virus, Bangladesh Bank had adopted and implemented an **expansionary and accommodative** monetary policy for FY 2020-21.

The main objective of MP20 was: (a) to accommodate and support government's target of achieving a qualitative high growth rate of GDP by means of economic activities (**GDP growth rate targeted: 8.2 pc**) and (b) to tame inflation rate to predetermined limit (**maximum inflation rate targeted: 5.4 pc**).

<u>**Objectives and Targets of MP20:**</u>

* To (a) **Increase the funds** of the banks for **loan disbursement** purposes and (b) to allow the banks to disburse the **loans at affordable rates** to the consumers, BB had taken these steps in MP20:

  * **Repo rate** decreased (at 50 points base) to **4.75 %** **Reverse Repo rate** decreased (at 75 points base) to **4.00 %** **Bank rate** decreased (at 100 points base) to **4.00 %**
  * **Loan moratorium** (stay-order of loan-repayments) was enacted for the banks
  * Ensured **continuing banking service despite lockdowns** and restrictions in movement due to the pandemic
  * Extended the **Usance Period of Import Loans** of necessary imports (especially raw materials of industries, agricultural machinery & chemical fertilizers).
  * Expanded the **re-financing facilities for the rebound of priority-sectors** of the economy - especially Agriculture, CMSME, Heavy Industries, Export Oriented Industries and Service. Credit Guaranty Scheme was also initiated for the purpose of re-financing of the CMSME sector.
  * Initiated a "**Start-Up Fund**" for the creation of Entrepreneurs and Self-Employed with **500 Crore BDT** from Bangladesh Bank and **1 pc of working profit** of the Scheduled Banks.
  * Initiated a "**Technological Upgradation Fund**" with **BDT 1100 Crore** to increase the productivity of Export Oriented Industries.
  * Took measures accordingly to **implement** government's **28 stimulus packages of BDT 1.35 Trillion Crore**, including 12 packages for the banking sector
* All these efforts along with **increase in Remittance inflow** accumulated to a **positive and dynamic economy in the first quarter** of FY2020-21.

<u>**The Second Wave:**</u>

But then the **Second Wave** of Coronavirus grasped the world and eventually permeated into Bangladesh that later hit it hard.

* Being hit hard by the Second Wave, government **decreased the target** of **GDP growth rate from 8.2 pc to 7.4 pc**. But the targeted **inflation rate was unchanged** (5.4 pc)
* Even BB also **amended the MP20** accordingly in the second half of the FY.
* Mid-March, Second Wave in full swing, government imposed **countrywide Lockdown from 5th April 2021** - the economy in a standstill.
* Growth rate of most variables in monetary and lending programmes worsened during that period. Especially, **Lending in Private Sector was the worst in performance**; a very low **8.4 pc growth in lending rate** was performed in this sector **against a projection of 14.8 pc**.
* During that period, the government also took several huge loans. But those were **mostly from non-bank sources (eg., Sanchay Patra etc).** Due to this low rate of lending to government from the banks, as well as **high influx of remittance flow** resulted in **outstanding increase in cash and liquid assets in the banking sector**.
* The amount of **Broad Money** in the market were **diminished** because of the **slower growth of borrowing** from the public-private sectors and the government. This was somewhat **eased** the task of **taming the growth of inflation**.

<u>**Foreign Reserve:**</u>

* **High Remittance Influx:** Government encouraged expatriates' sending money to Bangladesh through the banking channel by **incentivizing with 2 pc cash stimulus**. Bangladesh bank also took and implemented necessary steps and policies to **develop infrastructures, including MFS**(Mobile Financial Services), to help achieve the goal of high remittance inflow. For these, after FY20-21, the **growth rate of remittance income increased** an astounding **36.1 pc**.
* **Export Income:** During the strict lockdown, government **allowed keeping factories open**. Bangladesh Bank implemented a policy to allow the factories to **take loans** from a new fund called **Export Development Fund (EDF)**. Therefore, export income were able to ward off the negative impacts of the second wave and after FY20-21, **growth rate** of them stood at **15.1 pc**.

<u>**Economic Sectors of Bangladesh during FY20-21:**</u>

* Some **specific sub-sectors of the service sector** had been **hurt worst** due to the **pandemic**. That **obstructed** gaining a **high-quality GDP growth** during FY20-21.
* But the **Agriculture and Industry sectors** played much needed **important role** in retaining a **somewhat acceptable gain** of approximately **6.1 pc in GDP growth** (*temporary account*); which is **not much afar from** the growth rate **projected** in MP20.

<u>**Commodity Prices:**</u>

* In the **first half of FY20-21**, the prices of **essential goods** were **in the rise**. This was because - **(a)** of **reopening of developed economies** due to **rapid vaccination efforts** and thus the **subsequent rise** in **demands** of **fossil fuel** and **consumer goods** which resulted in the price hike in the international market; (b) Cyclone **Amfan** in *May 2020* and **flash-flooding in northern Bengal** in *July 2020* resulted in much **damage** of crops.
* It became a **challenge**, then, to **limit the rise of inflation** to the targeted percentage, amid the price hike.
* But then again, a proper **collaboration** between **BB** and the **NBR** was able to tackle the problem at hand. **NBR** **reduced** the amount of **import tax** in food and other essential goods, whereas **BB** **reduced** the **LC Margin** (= Letter of Credit Margin) requirement for such imports.
* That's how the inflation rate was tamed during the price hike.

<u>**Inflation:**</u>

* As the **prices** of **edible oil, spices, sugar and rice** was on the **high**-end, the **inflation** rate **in Food sub-index** was **high**, making the **overall inflation** rate **a bit higher than the projected** (in MP20) inflation rate of 5.4 pc.

<u>**Successes of Monetary Policy 2020-21:**</u>

* **External Sectors** returned to **normal**
* Was able to **lower** the **deficiency** in **Balance in Foreign Exchange**.FY19-20: **BoP was 4727 million USD** (negative). FY20-21: **BoP 3808 million USD** (negative)
* Good **surplus** was gained in the **overall Balance of Payments**. In spite of 3808 negative foreign exchange balance, Bangladesh was able to put up a **BoP of positive 9278 million USD**. This was **because** of (a) Foreign Direct Investment (**FDI**) and (b) **loans** from our **development partners**.
* **Value of Taka** vis-a-vis US Dollar was kept at a **constant** rate.

## Now the Goals, Stance and Fiscal & Loan Programmes in the current *FY2021-22 Monetary Policy*

**Liquidity and liquid assets** have **increased** drastically in the **banking sector** due to **expansionary** taxation and monetary policy. But the macro-economy has not yet been able to rebound enough from the state it had been due to the pandemic. In this circumstance, BB has adopted this MP21 in such stance, and with such programmes so as to achieve the **government targeted 7.2 pc GDP growth rate and 5.3 pc inflation**.

<u>**Programs and Targets of MP21:**</u>

* **Safe Limit** for **rate of increase in Broad Money (M-2) supply** has been determined to be **15.0 pc**.
* This is because:

  * **Income Velocity of Money** had been **downwards** for the **last couple of years**, due to people's tendency to hold money in liquid due to the uncertain pandemic times they had been going through
* To tame the Broad Money Supply within the safe limit, a **17.8 pc growth rate** for **internal lending** has been determined.

  * of them, **growth in government lending: 765 billion Taka (36.6 pc)** and
  * **growth in private lending: 1960 billion Taka (14.8 pc)**
* So that, the

  * **growth in net internal asset** in banking sector becomes: **16.5 pc** and
  * **growth in net foreign asset** in banking sector becomes: **10.4 pc**
* The recent **price hike** of most products including fuel will result in **increase** in **import costs**. So it has been **projected** in MP21 that the **growth in net foreign asset will be lesser than the previous FY**.
* **Export Income** targeted growth rate: **13.0 pc**
* **Import Income** targeted growth rate: **13.5 pc**
* **Remittance Influx** targeted growth rate: **20.0 pc**
* **FX Reserve (projected after FY21-22): 52 billion USD**

  * which is enough to fulfill the internal demand of the country for a consecutive **7.1 month**.

<u>**Policy Related Aspects of the Current MP21:**</u>

* The effects of the Second Wave had not yet been gone from the economy. So BB put great importance in **continuing the programs** taken in MP20 that aimed to ease the economy's regaining normality, eg., decrease in CRR and so on.
* **Expansionary and Accommodative policy** taken, again.
* Taken a **stance** on **lesser lending to unproductive** or less productive sectors and **increasing loan** disbursement to **productive sectors**.

<u>**Programs taken:**</u>

* **Strict measures** to be taken by BB to **ensure proper implementation** of government's **stimulus packages**.
* **Expansion** of the **Re-financing Scheme**, **taken** by BB **last FY**, for the **improvement** in **priority-based sectors** of the economy. The sectors include - Agriculture, CMSME, Heavy Industries, Export Oriented Industries and Service.
* **Special Re-financing Scheme program** has been **started** this FY for the improvement in **non-institutionalized sectors** of the economy. The sectors include - Small Businessmen, Transport Workers, Hotel and Restaurant Workers and Private School/College Teachers.
* **Step by step expansion** of the **Start-Up-Fund** of **BB's 500 Crore** BDT and **1 pc** of the **working profit** of the **scheduled banks**.
* **Functional inauguration** of **BB's Credit Guaranty Scheme** for the development of **CMSMEs**, especially **Light Engineering, Cluster & Value Chain** and **Women Entrepreneurs**.
* **Emphasizing** the banks' opening up of **sub-branches** in **remote village** areas to achieve the target of **Financial Inclusion** and **Quality Employment**.



## Infographic Depiction

![Prothomalo's Infographic of MP21](/images/1.jpg)
